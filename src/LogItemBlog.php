<?php

/**
 * Created by PhpStorm.
 * User: owner
 * Date: 29/08/2016
 * Time: 9:23 AM
 */
class LogItemBlog extends LogItem
{
    protected $content;

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }
}