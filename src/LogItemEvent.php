<?php

/**
 * Created by PhpStorm.
 * User: owner
 * Date: 29/08/2016
 * Time: 9:17 AM
 */
class LogItemEvent extends LogItem
{
    protected $eventType;
    protected $location;
    protected $requiresRegistration;

    public function getEventType()
    {
        return $this->eventType;
    }

    public function setEventType($type)
    {
        $this->eventType = $type;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getRequiresRegistration()
    {
        return $this->requiresRegistration;
    }

    public function setRequiresRegistration($req)
    {
        $this->requiresRegistration = $req;
    }
}