<?php

/**
 * Created by PhpStorm.
 * User: owner
 * Date: 29/08/2016
 * Time: 9:13 AM
 */
abstract class LogItem
{
    protected $id;
    protected $date;
    protected $title;
    protected $description;

    public function getId()
    {
        return $this->id;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }
}