<?php
/** @Embeddable */
class Address
{
    /** @Column(type = "string") */
    protected $street;

    /** @Column(type = "string") */
    protected $postalCode;

    /** @Column(type = "string") */
    private $city;

    /** @Column(type = "string") */
    private $country;

    public function __construct($str, $pc, $city, $cntry)
    {
        $this->street = $str;
        $this->postalCode = $pc;
        $this->city = $city;
        $this->country = $cntry;
    }
}