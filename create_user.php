<?php
// create_user.php
require_once "bootstrap.php";

$newUsername = $argv[1];

$user = new User();
$user->setName($newUsername);

$addr = new Address("Baxter Street", "3021", "Melbourne", "Australia");
$user->setAddress($addr);

echo "Before persist(): " . $entityManager->getUnitOfWork()->getEntityState($user) . PHP_EOL;
$entityManager->persist($user);
echo "After persist() but before flush(): " . $entityManager->getUnitOfWork()->getEntityState($user) . PHP_EOL;
$entityManager->flush();
echo "After flush(): " . $entityManager->getUnitOfWork()->getEntityState($user) . PHP_EOL;

echo "Created User with ID " . $user->getId() . "\n";

$entityManager->remove($user);
echo "After remove(): " . $entityManager->getUnitOfWork()->getEntityState($user) . PHP_EOL;