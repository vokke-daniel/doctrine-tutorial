<?php
// create_product.php
require_once "bootstrap.php";

$newEventName = $argv[1];

$event = new LogItemEvent();
$event->setTitle("Cool Event");
$event->setDescription("Very Cool");
$event->setEventType("Cool");

$entityManager->persist($event);
$entityManager->flush();

echo "Created Event with ID " . $event->getId() . "\n";